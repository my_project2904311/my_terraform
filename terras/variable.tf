terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.40.0"
    }
  }
}

provider "aws" {
  region                   = var.region
  shared_config_files      = ["/root/.aws/config"]
  shared_credentials_files = ["/root/.aws/credentials"]
}

resource "aws_instance" "my_instance" {
  ami           = var.ami
  instance_type = var.instance_type
  key_name      = var.key_name
  tags          = var.tags
}

variable "region" {
  type        = string
  default     = "ap-south-1"
  description = "variable region"
}
variable "ami" {
  type        = string
  description = "ami image"
  default     = "ami-03bb6d83c60fc5f7c"
}

variable "instance_type" {
  type        = string
  default     = "t2.micro"
  description = "variable instance_type"
}
variable "key_name" {
  type        = string
  default     = "mmmyyy"
  description = "variable key-name"
}

variable "tags" {
  type        = map(any)
  description = "variable region"
  default = {
    Name      = "ubuntu from_terra"
    envorment = "staging"
    project   = "xyz"
  }
}
output "completed" {
  value = "yes"
}
