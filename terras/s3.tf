terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.40.0"
    }
  }
}
provider "aws" {
  region                  = var.region
  shared_config_files      = ["/root/.aws/config"]
  shared_credentials_files = ["/root/.aws/credentials"]
}

resource "aws_s3_bucket" "my_buuuuucket" {
  bucket = var.bucket_name
  tags = var.tags
}

variable "region" {
  type        = string
  default     = "ap-south-1"
  description = "this is region"
}

variable "bucket_name" {
  type        = string
  description = "Name of the S3 bucket"
  default     = "my-unique-bucket-namenamename"
}

variable "bucket_acl" {
  type        = string
  description = "ACL for the S3 bucket"
  default     = "private"
}

variable "tags" {
  type        = map
  description = "Tags for the S3 bucket"
  default     = {
    Name        = "my-buuuuucket"
    environment = "staging"
    project     = "xyz"
  }
}

output "completed" {
  value = "yes"
}