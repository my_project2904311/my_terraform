 region = "us-west-2"

 ami = "ami-0f7197c592205b389"

 instance_type = "t2.medium"

 key_name = "windows_global_key"

# root@ip-172-31-45-73:/terraform/my_terraform/staging-terraform/EC2# cat variable.tf 
# variable "region" {
#   type = string
#   description = "providing my refion for project"
# }

# variable "ami" {
#   type = string
#   description = "providing ami id to server"
# }

# variable "instance_type" {
#   type = string
#   description = "providing the server size"
# }

# variable "key_name" {
#   type = string
#   description = "providing my server key"